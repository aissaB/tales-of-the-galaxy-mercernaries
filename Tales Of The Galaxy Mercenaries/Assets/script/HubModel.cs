﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

public class HubModel : MonoBehaviour
{
    public List<quest> allQuest;
    public List<Unit> AvailibleRecruits=new List<Unit>();
    public int nbRecruits;

    public List<Color> skinTones;
    public List<Sprite> Portraits_Hair;
    public List<Sprite> Portraits_Face;
    public List<Sprite> Portraits_Body;
    private readonly List<string> NounsPseudos = new List<string>() { "Aardvark", "Abyssinian", "Adelie Penguin", "Affenpinscher", "Afghan Hound", "African Bush Elephant", "African Civet", "African Clawed Frog", "African Forest Elephant", "African Palm Civet", "African Penguin", "African Tree Toad", "African Wild Dog", "Ainu Dog", "Airedale Terrier", "Akbash", "Akita", "Alaskan Malamute", "Albatross", "Aldabra Giant Tortoise", "Alligator", " Dachsbracke", " Bulldog", " Cocker Spaniel", " Coonhound", " Eskimo Dog", " Foxhound", " Pit Bull Terrier", " Staffordshire Terrier", " Water Spaniel", "Amur Leopard", " Shepherd Dog", "Angelfish", "Ant", "Anteater", "Antelope", "Appenzeller Dog", "Arctic Fox", "Arctic Hare", "Arctic Wolf", "Armadillo", " Elephant", " Giant Hornet", " Palm Civet", " Black Bear", "Aurochs", " Cattle Dog", " Kelpie Dog", " Mist", " Shepherd", " Terrier", "Avocet", "Axolotl", "Aye Aye", "Baboon", "Bactrian Camel", "Badger", "Balinese", "Banded Palm Civet", "Bandicoot", "Barb", "Barn Owl", "Barnacle", "Barracuda", "Basenji Dog", "Basking Shark", "Basset Hound", "Bat", "Mountain Hound", "Beagle", "Bear", "Bearded Collie", "Bearded Dragon", "Beaver", "Bedlington Terrier", "Beetle", "Bengal Tiger", "Bernese Mountain Dog", "Bichon Frise", "Binturong", "Bird", "Birds Of Paradise", "Birman", "Bison", "Black Rhinoceros", "Black Russian Terrier", "Black Widow Spider", "Blobfish", "Bloodhound", "Blue Lacy Dog", "Blue Whale", "Bluetick Coonhound", "Bobcat", "Bolognese Dog", "Bombay", "Bongo", "Bonobo", "Booby", "Border Collie", "Border Terrier", "Bornean Orang-utan", "Borneo Elephant", "Boston Terrier: Complete Pet Guide", "Bottlenose Dolphin", "Boxer Dog", "Boykin Spaniel", "Brazilian Terrier", "Brown Bear", "Budgerigar", "Buffalo", "Bull Mastiff", "Bull Shark", "Bull Terrier", "Bulldog", "Bullfrog", "Bumble Bee", "Burmese", "Burrowing Frog", "Butterfly", "Butterfly Fish", "Caiman", "Caiman Lizard", "Cairn Terrier", "Camel", "Camel Spider", "Canaan Dog", "Capybara", "Caracal", "Carolina Dog", "Cassowary", "Cat", "Caterpillar", "Catfish", "Cavalier King Charles Spaniel", "Centipede", "Cesky Fousek", "Chameleon", "Chamois", "Cheetah", "Chesapeake Bay Retriever", "Chicken", "Chihuahua", "Chimpanzee", "Chinchilla", "Chinese Crested Dog", "Chinook", "Chinstrap Penguin", "Chipmunk", "Chow Chow", "Cichlid", "Clouded Leopard", "Clown Fish", "Clumber Spaniel", "Coati", "Cockatoo", "Cockroach", "Collared Peccary", "Collie", "Common Buzzard", "Common Frog", "Common Loon", "Common Toad", "Coral", "Cottontop Tamarin", "Cougar", "Cow", "Coyote", "Crab", "Crab-Eating Macaque", "Crane", "Crested Penguin", "Crocodile", "Cross River Gorilla", "Curly Coated Retriever", "Cuscus", "Cuttlefish", "Dachshund", "Dalmatian", "Darwin’s Frog", "Deer", "Desert Tortoise", "Deutsche Bracke", "Dhole", "Dingo", "Discus", "Doberman Pinscher", "Dodo", "Dog", "Dogo Argentino", "Dogue De Bordeaux", "Dolphin", "Donkey", "Dormouse", "Dragonfly", "Drever", "Duck", "Dugong", "Dunker", "Dusky Dolphin", "Dwarf Crocodile", "Eagle", "Earwig", "Eastern Gorilla", "Eastern Lowland Gorilla", "Echidna", "Edible Frog", "Egyptian Mau", "Electric Eel", "Elephant", "Elephant Seal", "Elephant Shrew", "Emperor Penguin", "Emperor Tamarin", "Emu", "English Cocker Spaniel", "English Shepherd", "English Springer Spaniel", "Entlebucher Mountain Dog", "Epagneul Pont Audemer", "Eskimo Dog", "Estrela Mountain Dog", "Falcon", "Fennec Fox", "Ferret", "Field Spaniel", "Fin Whale", "Finnish Spitz", "Fire-Bellied Toad", "Fish", "Fishing Cat", "Flamingo", "Flat Coat Retriever", "Flounder", "Fly", "Flying Squirrel", "Fossa", "Fox", "Fox Terrier", "French Bulldog", "Frigatebird", "Frilled Lizard", "Frog", "Fur Seal", "Galapagos Penguin", "Galapagos Tortoise", "Gar", "Gecko", "Gentoo Penguin", "Geoffroys Tamarin", "Gerbil", "German Pinscher", "German Shepherd Guide", "Gharial", "Giant African Land Snail", "Giant Clam", "Giant Panda Bear", "Giant Schnauzer", "Gibbon", "Gila Monster", "Giraffe", "Glass Lizard", "Glow Worm", "Goat", "Golden Lion Tamarin", "Golden Oriole", "Golden Retriever Complete Pet Guide", "Goose", "Gopher", "Gorilla", "Grasshopper", "Great Dane", "Great White Shark", "Greater Swiss Mountain Dog", "Green Bee-Eater", "Greenland Dog", "Grey Mouse Lemur", "Grey Reef Shark", "Grey Seal", "Greyhound", "Grizzly Bear", "Grouse", "Guinea Fowl", "Hammerhead Shark", "Hamster", "Hare", "Harpy Eagle", "Harrier", "Havanese", "Hawaiian Crow", "Hedgehog", "Hercules Beetle", "Hermit Crab", "Heron", "Highland Cattle", "Himalayan", "Hippopotamus", "Honey Badger", "Honey Bee", "Hoopoe", "Horn Shark", "Horned Frog", "Horse", "Horseshoe Crab", "Howler Monkey", "Human", "Humboldt Penguin", "Hummingbird", "Humpback Whale", "Hyena", "Ibis", "Ibizan Hound", "Iguana", "Impala", "Indian Elephant", "Indian Palm Squirrel", "Indian Rhinoceros", "Indian Star Tortoise", "Indochinese Tiger", "Indri", "Insect", "Irish Setter Complete Pet Guide", "Irish WolfHound", "Jack Russel", "Jackal", "Jaguar", "Japanese Chin", "Japanese Macaque", "Javan Rhinoceros", "Javanese", "Jellyfish", "Jerboa", "Kakapo", "Kangaroo", "Keel Billed Toucan", "Killer Whale", "King Crab", "King Penguin", "Kingfisher", "Kiwi", "Koala", "Komodo Dragon", "Kudu", "Labradoodle – The Complete Guide For Owners", "Labrador Retriever", "Ladybug", "Leaf-Tailed Gecko", "Lemming", "Lemur", "Leopard", "Leopard Cat", "Leopard Seal", "Leopard Tortoise", "Liger", "Lion", "Lionfish", "Little Penguin", "Lizard", "Llama", "Lobster", "Long-Eared Owl", "Lynx", "Macaroni Penguin", "Macaw", "Magellanic Penguin", "Magpie", "Maine Coon", "Malayan Civet", "Malayan Tiger", "Maltese", "Manatee", "Mandrill", "Manta Ray", "Marine Toad", "Markhor", "Marsh Frog", "Masked Palm Civet", "Mastiff", "Mayfly", "Meerkat", "Megalodon", "Millipede", "Minke Whale", "Mole", "Molly", "Mongoose", "Mongrel", "Monitor Lizard", "Monkey", "Monte Iberia Eleuth", "Moorhen", "Moose", "Moray Eel", "Moth", "Mountain Gorilla", "Mountain Lion", "Mouse", "Mule", "Neanderthal", "Neapolitan Mastiff", "Newfoundland", "Newt", "Nightingale", "Norfolk Terrier", "North American Black Bear", "Norwegian Forest", "Numbat", "Nurse Shark", "Ocelot", "Octopus", "Okapi", "Old English Sheepdog", "Olm", "Opossum", "Orang-utan", "Ostrich", "Otter", "Oyster", "Pademelon", "Panther", "Parrot", "Patas Monkey", "Peacock", "Pekingese", "Pelican", "Penguin", "Pere Davids Deer", "Persian", "Pheasant", "Pied Tamarin", "Pig", "Pika", "Pike", "Pink Fairy Armadillo", "Piranha", "Platypus", "Pointer", "Poison Dart Frog", "Polar Bear", "Pond Skater", "Poodle", "Pool Frog", "Porcupine", "Porpoise", "Possum", "Prawn", "Proboscis Monkey", "Puffer Fish", "Puffin", "Pug", "Puma", "Purple Emperor", "Puss Moth", "Pygmy Hippopotamus", "Pygmy Marmoset", "Quail", "Quetzal", "Quokka", "Quoll", "Rabbit", "Raccoon", "Raccoon Dog", "Radiated Tortoise", "Ragdoll", "Rat", "Rattlesnake", "Red Knee Tarantula", "Red Panda", "Red Wolf", "Red-handed Tamarin", "Reindeer", "Rhinoceros", "River Dolphin", "River Turtle", "Robin", "Rock Hyrax", "Rockhopper Penguin", "Roseate Spoonbill", "Rottweiler", "Royal Penguin", "Russian Blue", "Sabre-Toothed Tiger", "Saint Bernard", "Salamander", "Sand Lizard", "Saola", "Scimitar-horned Oryx", "Scorpion", "Scorpion Fish", "Sea Dragon", "Sea Lion", "Sea Otter", "Sea Slug", "Sea Squirt", "Sea Turtle", "Sea Urchin", "Seahorse", "Seal", "Serval", "Sheep", "Shih Tzu", "Shrimp", "Siamese", "Siamese Fighting Fish", "Siberian", "Siberian Husky", "Siberian Tiger", "Silver Dollar", "Skunk", "Sloth", "Slow Worm", "Snail", "Snake", "Snapping Turtle", "Snowshoe", "Snowy Owl", "Somali", "South China Tiger", "Spadefoot Toad", "Sparrow", "Spectacled Bear", "Sperm Whale", "Spider Monkey", "Spiny Dogfish", "Spixs Macaw", "Sponge", "Squid", "Squirrel", "Squirrel Monkey", "Sri Lankan Elephant", "Staffordshire Bull Terrier", "Stag Beetle", "Starfish", "Stellers Sea Cow", "Stick Insect", "Stingray", "Stoat", "Striped Rocket Frog", "Sumatran Elephant", "Sumatran Orang-utan", "Sumatran Rhinoceros", "Sumatran Tiger", "Sun Bear", "Swan", "Tang", "Tapanuli Orang-utan", "Tapir", "Tarsier", "Tasmanian Devil", "Tawny Owl", "Termite", "Tetra", "Thorny Devil", "Tibetan Mastiff", "Tiffany", "Tiger", "Tiger Salamander", "Tiger Shark", "Tortoise", "Toucan", "Tree Frog", "Tropicbird", "Tuatara", "Turkey", "Turkish Angora", "Uakari", "Uguisu", "Umbrellabird", "Vampire Bat", "Vervet Monkey", "Vulture", "Wallaby", "Walrus", "Warthog", "Wasp", "Water Buffalo", "Water Dragon", "Water Vole", "Weasel", "Welsh Corgi: The Complete Pet Guide", "West Highland Terrier", "Western Gorilla", "Western Lowland Gorilla", "Whale Shark", "Whippet", "White Faced Capuchin", "White Rhinoceros", "White Tiger", "Wild Boar", "Wildebeest", "Wolf", "Wolf Spider", "Wolverine", "Wombat", "Woodlouse", "Woodpecker", "Woolly Mammoth", "Woolly Monkey", "Wrasse", "Wyoming Toad", "Xerus", "Yak", "Yellow-Eyed Penguin", "Yorkshire Terrier", "Zebra", "Zebra Shark", "Zebu", "Zonkey", "Zorse" };
    private readonly List<string> AdjectivesPseudos = new List<string>() { "ablaze", "ablazing", "accented", "achromatic", "ashen", "ashy", "atomic", "beaming", "bi-color", "blazing", "bleached", "bleak", "blended", "blotchy", "bold", "brash", "bright", "brilliant", "burnt", "checkered", "chromatic", "classic", "clean", "colored", "colorful", "colorless", "complementing", "contrasting", "cool", "coordinating", "crisp", "dappled", "dark", "dayglo", "deep", "delicate", "digital", "dim", "dirty", "matte", "medium", "mellow", "milky", "mingled", "mixed", "monochromatic", "motley", "mottled", "muddy", "multicolored", "multihued", "murky", "natural", "neutral", "opalescent", "opaque", "pale", "pastel", "patchwork", "patchy", "patterned", "perfect", "picturesque", "plain", "primary", "prismatic", "psychedelic", "pure", "radiant", "reflective", "rich", "royal", "ruddy", "rustic", "satiny", "saturated", "secondary", "shaded", "dotted", "drab", "dreary", "dull", "dusty", "earth", "electric", "eye-catching", "faded", "faint", "festive", "fiery", "flashy", "flattering", "flecked", "florescent", "frosty", "full-toned", "glistening", "glittering", "glowing", "harsh", "hazy", "hot", "hued", "icy", "illuminated", "incandescent", "intense", "interwoven", "iridescent", "kaleidoscopic", "lambent", "light", "loud", "luminous", "lusterless", "lustrous", "majestic", "shining", "shiny", "shocking", "showy", "smoky", "soft", "solid", "somber", "soothing", "sooty", "sparkling", "speckled", "stained", "streaked", "streaky", "striking", "strong neutral", "subtle", "sunny", "swirling", "tinged", "tinted", "tonal", "toned", "traditional", "translucent", "transparent", "two-tone", "undiluted", "uneven", "uniform", "vibrant", "vivid", "wan", "warm", "washed-out", "waxen", "wild", "abusive", "adulterous", "alcoholic", "angry", "annoying", "argumentative", "arrogant", "at fault", "atrocious", "awful", "backstabbing", "bad", "bat-shit crazy", "beyond reproach", "bitchy", "bitter", "boring", "brainless", "calculating", "careless", "caught", "chauvinistic", "cheap", "cheating", "childish", "cold", "cold-hearted", "common", "complicated", "confrontational", "conniving", "contemptible", "controlling", "corrupt", "cowardly", "crappy", "crazed", "crazy", "creepy", "criminal", "cruel", "cruel-hearted", "crummy", "crushing", "cursed", "deceitful", "deceiving", "deplorable", "depressing", "dickish", "dimwitted", "dirty", "disappointing", "disgraceful", "disgusting", "dishonest", "distressed", "disturbed", "disturbing", "double-crossing", "loose", "lost", "lousy", "low", "lowlife", "lying", "mad", "malicious", "maniacal", "manipulating", "manipulative", "mean", "mental", "miserable", "mistaken", "moody", "moronic", "narrow-minded", "nasty", "naughty", "nauseating", "no-good", "obnoxious", "offensive", "out-of-control", "out-of-line", "outraged", "painful", "pathetic", "pea-brained", "pissed", "pissed off", "pointless", "promiscuous", "psycho", "pushy", "rabid", "racist", "reckless", "reprehensible", "repulsive", "resentful", "ridiculous", "rotten", "rude", "sad", "saddened", "sadistic", "scared", "screwed-up", "self-absorbed", "self-centered", "self-consumed", "self-entitled", "self-inflated", "selfish", "shady", "shallow", "shameful", "shameless", "shitty", "sick", "dull", "dumb", "eccentric", "egotistical", "embarrassing", "embittered", "emotional", "empty", "evil", "exploiting", "fake", "false", "fat", "flawed", "foolish", "forgetful", "freak", "freeloading", "friendless", "fugly", "full of rage", "furious", "gold-digging", "gossipy", "greedy", "gross", "grouchy", "guilty", "halfwitted", "harmful", "hateful", "heartbreaking", "heinous", "hellish", "hideous", "horrible", "humiliating", "hurtful", "hurting", "idiotic", "ignorant", "ill-tempered", "immature", "immoral", "impatient", "inadequate", "inappropriate", "inexcusable", "infuriated", "insane", "insecure", "insensitive", "insincere", "irate", "irrational", "irresponsible", "irritating", "jealous", "lame", "lazy", "silly", "sleazy", "slutty", "smelly", "smutty", "sneaky", "sorry", "spiteful", "spoiled", "stealing", "stinky", "stupid", "superficial", "swindling", "tasteless", "terrible", "territorial", "thick", "thieving", "thoughtless", "ticked off", "tiny-dick", "trashy", "troubled", "twisted", "two-dimensional", "two-faced", "ugly", "unacceptable", "unapologetic", "undependable", "underhanded", "unethical", "unfair", "unforgiving", "ungrateful", "unhappy", "unjustifiable", "unlovable", "unreliable", "unthoughtful", "untrue", "untruthful", "unworthy", "useless", "vacuous", "vengeful", "verbally abusive", "vindictive", "violent", "weak", "weird", "whiny", "white trash", "wicked", "witless", "worthless", "wretched", "wrong", "agile", "ample", "angular", "anorexic", "awkward", "barrel-chested", "big", "big-bellied", "bodily", "bony", "brawny", "brisk", "broad", "bulbous", "bulging", "busty", "buxom", "calloused", "chubby", "chunky", "colossal", "compact", "corpulent", "curvy", "dainty", "defined", "delicate", "developed", "dimpled", "distended", "drooping", "dumpy", "dwarfish", "elephantine", "elfin", "elongated", "emaciated", "fast", "fat", "firm", "fit", "flabby", "narrow", "nimble", "obese", "overweight", "paunchy", "peg-legged", "petite", "plodding", "plump", "podgy", "ponderous", "portly", "potbellied", "protruding", "pudgy", "puny", "quick", "rotund", "rounded", "runty", "sawed-off", "scrawny", "sculpted", "shrimpy", "shriveled", "shrunken", "sinewy", "sizable", "skeletal", "skinny", "slender", "slim", "slinky", "slouched", "slow", "sluggish", "small", "small-waisted", "soft", "solid", "spindly", "spiny", "fleshy", "flexible", "frail", "full", "full-grown", "gangly", "gargantuan", "generous", "giant", "gigantic", "goliath", "graceful", "growing", "heavy", "herculean", "huge", "hulking", "humpbacked", "hunched", "hurried", "husky", "immense", "itsy-bitsy", "jumbo", "lanky", "large", "lean", "leggy", "limber", "limp", "lithe", "little", "loitering", "long-legged", "long-limbed", "lumbering", "lumpy", "mammoth", "massive", "measly", "meaty", "mighty", "square", "squat", "stacked", "starved", "stiff", "stocky", "stout", "strapping", "streamlined", "stubby", "stumpy", "stunted", "substantial", "supple", "svelte", "sweating", "swift", "swift-moving", "tall", "teeny", "thick", "thickset", "thin", "toned", "top-heavy", "towering", "tremendous", "trim", "tubby", "uncoordinated", "underfed", "undersized", "underweight", "unhurried", "voluptuous", "wee", "wide", "willowy", "wiry", "withered", "wobbly" };
    private QuestPreapation Qp;
    public List<EquipementItem> Items_Vendor;
    public List<EquipementItem> Items_Player;
    public List<Unit> Player_units;
    public List<Unit> Player_squad;
    public List<Unit> unitsInPasements;
    public List<Unit> unitsInFirstaid;
    public List<Unit> unitsInPaliatif;
    public List<Unit> unitsInChiurgie;
    public List<Unit> unitsInExperimental;
    private void Awake()
    {
        for (int i = 0; i < skinTones.Count; i++)
        {
            var tempColor = skinTones[i];
            tempColor.a = 1f;
            skinTones[i] = tempColor;
        }
    }
    public void GenerateRecruits() {
        for (int i = 0; i < nbRecruits; i++)
        {
            
            AvailibleRecruits.Add(ScriptableObject.CreateInstance<Unit>());
        }
    }
    public int GetAvatarPart(int whichPart) {
        switch (whichPart)
        {
            default:
                return Random.Range(0, Portraits_Hair.Count);
            case 0:
                return Random.Range(0, Portraits_Hair.Count);
            case 1:
                return Random.Range(0, Portraits_Body.Count);
            case 2:
                return Random.Range(0, Portraits_Face.Count);
            case 3:
                return Random.Range(0, skinTones.Count);
        }
        
    }
    public string GetCodeName() {
        return  AdjectivesPseudos[Random.Range(0,AdjectivesPseudos.Count)]+" "+NounsPseudos[Random.Range(0, NounsPseudos.Count)];
    }
    public void SaveQuestPrepartion(List<Unit> squad, quest Q)
    {
        Qp = new QuestPreapation(squad, Q);
        if (!Directory.Exists(@"" + Directory.GetCurrentDirectory() + "/gameData/CurrentQuest.data"))
        {
            Directory.CreateDirectory(@"" + Directory.GetCurrentDirectory() + "/gameData");

        }
        if (File.Exists(@"" + Directory.GetCurrentDirectory() + "/gameData/CurrentQuest.data"))
        {
            File.Delete(@"" + Directory.GetCurrentDirectory() + "/gameData/CurrentQuest.data");
        }
        IFormatter form = new BinaryFormatter();
        Stream s = new FileStream(@"" + Directory.GetCurrentDirectory() + "/gameData/CurrentQuest.data", FileMode.Create, FileAccess.Write);
        form.Serialize(s, Qp);
        s.Dispose();
    }
    public Color GetColorOfClass(Unit u) {
        if (u!=null)
        {
            switch (u.UnitClass)
            {
                case Unit.Class.Operateur:
                    return new Color32(132, 94, 194, 100);
                case Unit.Class.Scout:
                    return new Color32(18, 78, 120, 100);
                case Unit.Class.Gunner:
                    return new Color32(153, 104, 136, 100);
                case Unit.Class.Medic:
                    return new Color32(135, 203, 172, 100);
                case Unit.Class.Mechanic:
                    return new Color32(19, 117, 71, 100);
                case Unit.Class.Grenadier:
                    return new Color32(244, 44, 4, 100);
                case Unit.Class.Sniper:
                    return new Color32(246, 174, 45, 100);
                case Unit.Class.Pilot:
                    return new Color32(244, 0, 0, 100);
                case Unit.Class.Vehicules:
                    return new Color32(67, 40, 28, 100);
                default:
                    return new Color32(132, 94, 194, 100);
            }
        }
        else
        {
            return Color.white;
        }
        
    }

}
