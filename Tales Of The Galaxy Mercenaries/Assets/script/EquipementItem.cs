﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName = "Hub/Item")]
public class EquipementItem : ScriptableObject
{
    public Sprite itemSprite;
    public bool passif;
    public string itemName;
    public string description;
    public int price;
    public enum Class {All, Operateur, Scout, Gunner, Medic, Mechanic, Grenadier, Sniper, Pilot, Vehicules }
    public Class ItemClass = Class.All;
}
