﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;
public class fuckingshit : MonoBehaviour
{
    public Tilemap tilemap_ground;
    public Tilemap tilemap_Obstacles;
    public Tilemap tilemap_Environement;
    public GameObject joueur;
    Vector3 currentPosition;
    List<Vector3> oldPositions;
    List<Vector3> possiblePostion;
    public int StepsNb;
    // Start is called before the first frame update
    void Start()
    {

        joueur = this.gameObject;
        currentPosition = Vector3Int.CeilToInt(transform.position) + new Vector3(-0.5f, -0.5f, 0);
        transform.position = currentPosition;
        possiblePostion= new List<Vector3>();
        oldPositions = new List<Vector3>();
        ShowAllAvailablesMoves();
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetMouseButtonDown(0))
        {
            Vector3 mouspos= Camera.main.ScreenToWorldPoint(Input.mousePosition);
            Vector3 coordinates= tilemap_ground.GetCellCenterWorld(Vector3Int.CeilToInt(mouspos+new Vector3(-1,-1,0)));
            foreach (var item in possiblePostion)
            {
                if (coordinates == item)
                {
                    currentPosition = coordinates;
                    joueur.transform.position = coordinates;
                }
                else
                {
                    print("Coordinates: "+coordinates+"---- Possible : "+item);
                }
            }
            ShowAllAvailablesMoves();
        }
    }

    private void ShowAllAvailablesMoves() {
        possiblePostion.Clear();
        if (oldPositions.Count>0)
        {
            
            foreach (Vector3 tilePositions in oldPositions)
            {
             tilemap_ground.SetColor(Vector3Int.FloorToInt(tilePositions), Color.white);
            }
            print("effacé: "+oldPositions.Count);
            oldPositions.Clear();
        }
        bool stop1=false;
        bool stop2 = false;
        bool stop3 = false;
        bool stop4 = false;
        for (int i = 1; i < StepsNb+1; i++)
        {
            if (!stop1&& !tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(i, 0, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(i, 0, 0))))
            {

                possiblePostion.Add(currentPosition + new Vector3(i, 0, 0));

            }
            else
            {
                stop1 = true;
            }

            if (!stop2 && !tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(-i, 0, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(-i, 0, 0))))
            {

                possiblePostion.Add(currentPosition + new Vector3(-i, 0, 0));

            }
            else
            {
                stop2 = true;
            }

            if (!stop3 && !tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, i, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, i, 0))))
            {

                possiblePostion.Add(currentPosition + new Vector3(0, i, 0));

            }
            else
            {
                stop3 = true;
            }

            if (!stop4 && !tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, -i, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, -i, 0))))
            {

                possiblePostion.Add(currentPosition + new Vector3(0, -i, 0));

            }
            else
            {
                stop4 = true;
            }
        }

        //for (int i = -StepsNb; i < StepsNb+1; i++)
        //{
        //    if (i!=0)
        //    {
        //        if (!tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(i, 0, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(i, 0, 0))))
        //        {

        //            possiblePostion.Add(currentPosition + new Vector3(i, 0, 0));
                    
        //        }
        //        if (!tilemap_Obstacles.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, i, 0))) && tilemap_ground.HasTile(Vector3Int.FloorToInt(currentPosition + new Vector3(0, i, 0))))
        //        {
        //            possiblePostion.Add(currentPosition + new Vector3(0, i, 0));
        //        }
                
        //    }
            
        //}
        foreach (Vector3 tilePositions in possiblePostion)
        {
            tilemap_ground.SetTileFlags(Vector3Int.FloorToInt(tilePositions), TileFlags.None);
            tilemap_ground.SetColor(Vector3Int.FloorToInt(tilePositions), Color.red);
        }
        foreach (var item in possiblePostion)
        {
            oldPositions.Add(item);
        }
    }
}
