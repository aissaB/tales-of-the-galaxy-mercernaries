﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventorySlot : MonoBehaviour
{
    public EquipementItem ei;
    public bool HasItem;

    private void Awake()
    {
        HasItem = false;
    }
    public void AddItem(EquipementItem e) {
        ei = e;
      //  transform.GetChild(1).GetComponent<Image>().sprite = e.itemSprite;
        HasItem = true;
    }
    public void RemoveItem() {
        ei = null;
      //  transform.GetChild(1).GetComponent<Image>().sprite = null;
        HasItem = false;
    }
    
}
