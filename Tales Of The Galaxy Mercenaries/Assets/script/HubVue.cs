﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using System.Drawing;
using Color = UnityEngine.Color;
using System;

public class HubVue : MonoBehaviour
{
    #region General
    [Header("Pannels")]
    public GameObject ServicesPannel;
    public GameObject GamehudPannel;
    [Header("Buttons")]
    public Button BtnTakeQuest;
    public Button BtnTakeUnitInSquad;
    public Button BtnRemoveUnitInSquad;
    public Button BtnTakeRecruit;
    public Button BtnRemoveUnit;
    //IDS: all close 0; quest 1;Unit 2; Recruit 3; Logi buySell 4; Shop 5;
    public void OnServiceBtnClick(int id)
    {
        
        switch (id)
        {
            case 0:
                GamehudPannel.SetActive(true);
                ServicesPannel.SetActive(false);
                QuestPannel.SetActive(false);
                UnitsPannel.SetActive(false);
                RecruitPannel.SetActive(false);
                logisticPannel.SetActive(false);
                /**/ChoiceOfMenuPannel.SetActive(false);
                /**/BuySellPannel.SetActive(false);
                /**/MangementInventory.SetActive(false);
                HospitalPannel.SetActive(false);
                PrisonPannel.SetActive(false);
                GaragePannel.SetActive(false);
                break;
            case 1:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                QuestPannel.SetActive(!QuestPannel.activeSelf);
                break;
            case 2:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                UnitsPannel.SetActive(!UnitsPannel.activeSelf);
                break;
            case 3:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                RecruitPannel.SetActive(!RecruitPannel.activeSelf);
                break;
            case 4:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                logisticPannel.SetActive(!logisticPannel.activeSelf);
                ChoiceOfMenuPannel.SetActive(!ChoiceOfMenuPannel.activeSelf);
                break;
            case 5:
                ChoiceOfMenuPannel.SetActive(!ChoiceOfMenuPannel.activeSelf);
                BuySellPannel.SetActive(!BuySellPannel.activeSelf);
                break;
            case 6:
                ChoiceOfMenuPannel.SetActive(!ChoiceOfMenuPannel.activeSelf);
                MangementInventory.SetActive(!MangementInventory.activeSelf);
                break;
            case 7:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                HospitalPannel.SetActive(!HospitalPannel.activeSelf);
                break;
            case 8:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                PrisonPannel.SetActive(!PrisonPannel.activeSelf);
                break;
            case 9:
                GamehudPannel.SetActive(false);
                ServicesPannel.SetActive(true);
                GaragePannel.SetActive(!GaragePannel.activeSelf);
                break;
        }
    }
    public void BtnMangement(int idBtn, bool state) {
        switch (idBtn)
        {
            default:
                break;
            case 0:
                BtnTakeQuest.interactable = state;
                break;
            case 1:
                BtnTakeUnitInSquad.interactable = state;
                break;
            case 2:
                BtnRemoveUnitInSquad.interactable = state;
                break;
            case 3:
                BtnTakeRecruit.interactable = state;
                break;
            case 4:
                BtnRemoveUnit.interactable = state;
                break;
        }
    }
    public void UpdateEverythingUnits(List<Unit>PlayerUnits,HubController hubController) {
        UpdateUnitListPlayerUnitPannel(PlayerUnits, hubController);
        UpdateListUnitMedicBay(PlayerUnits, hubController);
    }
    #endregion
    [Header("Quests")]
    #region Quests
    public GameObject QuestPannel;
    public GameObject QuestBtnBluePrint;
    public GameObject AvailibleQuestPannel;
    public TMP_Text QuestDescription;

    public void UpdateDescriptionQuest(string desc) {
        QuestDescription.text = desc;
    }
    public void UpdateQuests(List<quest> GeneratedQuests,HubController hubController) {
        for (int i = 0; i < AvailibleQuestPannel.transform.childCount; i++)
        {
            GameObject.Destroy(AvailibleQuestPannel.transform.GetChild(i).gameObject);
        }
        foreach (quest item in GeneratedQuests)
        {
            GameObject newBtn = Instantiate(QuestBtnBluePrint, AvailibleQuestPannel.transform);
            newBtn.transform.GetChild(0).GetComponent<TMP_Text>().text = item.questName;
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.ShowQuestInfo(item));
        }
    }
    #endregion
    [Header("Recruitement")]
    #region Recruitement
    public GameObject RecruitPannel;
    public GameObject RecruitBtnBluePrint;
    public GameObject AvailibleRecruitPannel;
    public TMP_Text RecruitDescription;
    public void UpdateListRecruits(List<Unit> AvailibleRecruits,HubController hubController) {
        for (int i = 0; i < AvailibleRecruitPannel.transform.childCount; i++)
        {
            GameObject.Destroy(AvailibleRecruitPannel.transform.GetChild(i).gameObject);
        }
        foreach (var item in AvailibleRecruits)
        {
            GameObject newBtn = Instantiate(RecruitBtnBluePrint, AvailibleRecruitPannel.transform);
            newBtn.transform.GetChild(1).GetComponent<TMP_Text>().text = item.codeName;
            newBtn.transform.GetChild(0).GetComponent<Image>().color =              hubController.Model.GetColorOfClass(item);
            newBtn.transform.GetChild(0).GetChild(0).GetComponent<Image>().color =  hubController.Model.skinTones[item.skinTone];
            newBtn.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[item.Portrait_hair];
            newBtn.transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[item.Portrait_Face];
            newBtn.transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[item.Portrait_Body];
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.ShowRecruitInfo(item));
        }
    }
    
    public void UpdateDescriptionRecruit(string desc)
    {

        RecruitDescription.text = desc;
    }

    #endregion
    [Header("Unit")]
    #region Units
    public GameObject UnitsPannel;
    public GameObject UnitBtnBluePrint;
    public GameObject AvailibleUnitPannel;
    public TMP_Text UnitDescription;

    public void UpdateUnitListPlayerUnitPannel(List<Unit> PlayerUnits, HubController hubController) {
        for (int i = 0; i < AvailibleUnitPannel.transform.childCount; i++)
        {
            Destroy(AvailibleUnitPannel.transform.GetChild(i).gameObject);
        }
        foreach (var item in PlayerUnits)
        {
            GameObject newBtn = Instantiate(UnitBtnBluePrint, AvailibleUnitPannel.transform);
            newBtn.transform.GetChild(1).GetComponent<TMP_Text>().text = item.codeName;
            newBtn.transform.GetChild(0).GetChild(0).GetComponent<Image>().color  = hubController.Model.skinTones[item.skinTone];
            newBtn.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[item.Portrait_hair];
            newBtn.transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[item.Portrait_Face];
            newBtn.transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[item.Portrait_Body];
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.ShowUnitInfos(item));
        }
        

    }
    public void UpdateDescriptionUnit_PlayerUnitPannel(string tragicBackStory)
    {
        UnitDescription.text = tragicBackStory;
    }

    #endregion
    [Header("Squad")]
    #region Squad
    public GameObject squadIcons;
    public GameObject btnSquadBluePrint;
    public void UpdateSquad(List<Unit> Squad, HubController hubController) {
        foreach (Transform item in squadIcons.transform)
        {

            GameObject.Destroy(item.gameObject);
        }
        foreach (var item in Squad)
        {
            GameObject newBtn = Instantiate(btnSquadBluePrint, squadIcons.transform);
            newBtn.transform.GetChild(1).GetChild(0).GetComponent<Image>().color = hubController.Model.skinTones[item.skinTone];
            newBtn.transform.GetChild(1).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[item.Portrait_hair];
            newBtn.transform.GetChild(1).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[item.Portrait_Face];
            newBtn.transform.GetChild(1).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[item.Portrait_Body];
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.ClickOnSquadMember(item));
        }
        
    }
    #endregion
    [Header("Logistic")]
    #region Logistic
    public GameObject logisticPannel;
    public GameObject ChoiceOfMenuPannel;
    public GameObject BuySellPannel;
    public GameObject MangementInventory;
    public GameObject VendorInventoryPannel;
    public GameObject PlayerInventoryPannel_BuySell;
    public GameObject BtnInvendoryShop;
    public Image    itemVendorDetail_Image;
    public TMP_Text itemVendorDetail_Title;
    public TMP_Text itemVendorDetail_Description;
    public TMP_Text itemVendorDetail_Price;
    public Image    itemPlayerDetail_Image;
    public TMP_Text itemPlayerDetail_Title;
    public TMP_Text itemPlayerDetail_Description;
    public TMP_Text itemPlayerDetail_Price;
    public Button BuyButton;
    public Button SellButton;
    public GameObject BtnPlayerInventoryBlueprint;
    public GameObject ListItemInPlayerInventoryPannel;
    public GameObject ListUnitOfPlayerInventoryPannel;
    public Image UnitPortraitInventory;
    public TMP_Text UnitCodeNameInventory;
    public TMP_Text UnitDetailsInventory;
    public GameObject buttonItemEquipedByUnit_LogisticInventory_1;
    public GameObject buttonItemEquipedByUnit_LogisticInventory_2;
    public GameObject buttonItemEquipedByUnit_LogisticInventory_3;
    public GameObject buttonItemEquipedByUnit_LogisticInventory_4;

    public void UpdateItemsVendorSide(List<EquipementItem> ei,HubController hubController)
    {
        for (int i = 0; i < VendorInventoryPannel.transform.childCount; i++)
        {
            GameObject.Destroy(VendorInventoryPannel.transform.GetChild(i).gameObject);
        }
        foreach (EquipementItem item in ei)
        {
            GameObject newBtn = Instantiate(BtnInvendoryShop, VendorInventoryPannel.transform);
            newBtn.GetComponent<InventorySlot>().AddItem(item);
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.SelectedItemVendorList(item));
            newBtn.transform.GetChild(1).GetComponent<Image>().sprite = item.itemSprite;
        }
        for (int i = ei.Count; i < 16; i++)
        {
            GameObject newBtn = Instantiate(BtnInvendoryShop, VendorInventoryPannel.transform);
        }
    }
    public void UpdateItemsPlayerSide(List<EquipementItem> ei, HubController hubController)
    {
        for (int i = 0; i < PlayerInventoryPannel_BuySell.transform.childCount; i++)
        {
            GameObject.Destroy(PlayerInventoryPannel_BuySell.transform.GetChild(i).gameObject);
        }
        foreach (var item in ei)
        {
            GameObject newBtn = Instantiate(BtnInvendoryShop, PlayerInventoryPannel_BuySell.transform);
            newBtn.GetComponent<InventorySlot>().AddItem(item);
            newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.SelectedItemPlayerList(item));
            newBtn.transform.GetChild(1).GetComponent<Image>().sprite = item.itemSprite;
        }
        for (int i = ei.Count; i < 30; i++)
        {
            GameObject newBtn = Instantiate(BtnInvendoryShop, PlayerInventoryPannel_BuySell.transform);
        }
    }
    public void ShowItemDetailvendor(EquipementItem e)
    {
        itemVendorDetail_Image.sprite=e.itemSprite;
        itemVendorDetail_Title.text = e.itemName;
        itemVendorDetail_Description.text = e.description;
        itemVendorDetail_Price.text=e.price.ToString();
    }
    public void ShowItemDetailPlayerInventory(EquipementItem e) {
        itemPlayerDetail_Image.sprite = e.itemSprite;
        itemPlayerDetail_Title.text = e.itemName;
        itemPlayerDetail_Description.text = e.description;
        itemPlayerDetail_Price.text = e.price.ToString();
    }
    public void HideItemDetailVendor() {
        itemVendorDetail_Image.sprite = null;
        itemVendorDetail_Title.text = "";
        itemVendorDetail_Description.text = "";
        itemVendorDetail_Price.text = "";
    }
    public void HideItemDetailPlayerBuySell()
    {
        itemPlayerDetail_Image.sprite = null;
        itemPlayerDetail_Title.text = "";
        itemPlayerDetail_Description.text = "";
        itemPlayerDetail_Price.text = "";
    }
    public void UpdateListOfPlayerItemsLogistic(List<EquipementItem> items_Player,HubController hubController,Unit CurrentSelectedUnit)
    {
        for (int i = 0; i < ListItemInPlayerInventoryPannel.transform.childCount; i++)
        {
            GameObject.Destroy(ListItemInPlayerInventoryPannel.transform.GetChild(i).gameObject);
        }
        foreach (EquipementItem item in items_Player)
        {
            if (item.ItemClass==EquipementItem.Class.All || item.ItemClass.ToString()==CurrentSelectedUnit.UnitClass.ToString())
            {
                GameObject btnPlayerInventoryItem = Instantiate(BtnPlayerInventoryBlueprint, ListItemInPlayerInventoryPannel.transform);
                btnPlayerInventoryItem.GetComponent<InventorySlot>().AddItem(item);
                btnPlayerInventoryItem.transform.GetChild(0).GetComponent<Image>().sprite = item.itemSprite;
                btnPlayerInventoryItem.transform.GetChild(1).transform.GetChild(0).GetComponent<TMP_Text>().text = item.itemName;
                btnPlayerInventoryItem.transform.GetChild(1).transform.GetChild(1).GetComponent<TMP_Text>().text = item.description;
                btnPlayerInventoryItem.GetComponent<Button>().onClick.AddListener(() => hubController.SelectItemToEquip(item));
            }
        }
    }
    public void UpdateListOfUnitInLogistic(List<Unit> Player_Units,HubController hubController) {
        for (int i = 0; i < ListUnitOfPlayerInventoryPannel.transform.childCount; i++)
        {
            GameObject.Destroy(ListUnitOfPlayerInventoryPannel.transform.GetChild(i).gameObject);
        }
        
        foreach (var item in Player_Units)
        {
            GameObject btnPlayerUnitInInventory = Instantiate(UnitBtnBluePrint, ListUnitOfPlayerInventoryPannel.transform);
            btnPlayerUnitInInventory.transform.GetChild(0).GetComponent<Image>().color = hubController.Model.GetColorOfClass(item);
            btnPlayerUnitInInventory.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = hubController.Model.skinTones[item.skinTone];
            btnPlayerUnitInInventory.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[item.Portrait_hair];
            btnPlayerUnitInInventory.transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[item.Portrait_Face];
            btnPlayerUnitInInventory.transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[item.Portrait_Body];
            btnPlayerUnitInInventory.GetComponent<Button>().onClick.AddListener(() => hubController.ShowUnitInfosInventory(item));
        }
    }
    public void UpdateUnitInfosInventory(Color backgroundColor,Color skinTone,Sprite hair,Sprite face,Sprite body,string codeName,string Description) {
        UnitPortraitInventory.color = backgroundColor;
        UnitPortraitInventory.gameObject.transform.GetChild(0).GetComponent<Image>().color = skinTone;
        UnitPortraitInventory.gameObject.transform.GetChild(1).GetComponent<Image>().sprite = hair;
        UnitPortraitInventory.gameObject.transform.GetChild(2).GetComponent<Image>().sprite = face;
        UnitPortraitInventory.gameObject.transform.GetChild(3).GetComponent<Image>().sprite = body;

        UnitCodeNameInventory.text = codeName;
        UnitDetailsInventory.text = Description;
    }
    public void UpdateBtnEquippedItemByUnit(List<EquipementItem> Eis) {
        switch (Eis.Count)
        {
            default:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = null;
                break;
            case 0:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = null;
                break;
            case 1:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = Eis[0].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = null;
                break;
            case 2:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = Eis[0].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = Eis[1].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = null;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = null;
                break;
            case 3:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = Eis[0].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = Eis[1].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = Eis[2].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = null;
                break;
            case 4:
                buttonItemEquipedByUnit_LogisticInventory_1.transform.GetChild(0).GetComponent<Image>().sprite = Eis[0].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_2.transform.GetChild(0).GetComponent<Image>().sprite = Eis[1].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_3.transform.GetChild(0).GetComponent<Image>().sprite = Eis[2].itemSprite;
                buttonItemEquipedByUnit_LogisticInventory_4.transform.GetChild(0).GetComponent<Image>().sprite = Eis[3].itemSprite;
                break;
        }
    }
    #endregion
    [Header("Hospital")]
    #region Hospital
    public GameObject HospitalPannel;
    public GameObject PannelUnitList;

    public Image Portrait_details_MedicBay;
    public TMP_Text CodeNameAndDetails_Details_MedicBay;

    public List<GameObject> pansementUnitList;
    public List<GameObject> firstaidUnitList;
    public List<GameObject> paliatifCureUnitList;
    public List<GameObject> chiurgeryUnitList;
    public List<GameObject> experimentalUnitList;
    public void UpdateListUnitMedicBay(List<Unit> playerUnit,HubController hubController) {
        for (int i = 0; i < PannelUnitList.transform.childCount; i++)
        {
            GameObject.Destroy(PannelUnitList.transform.GetChild(i).gameObject);
        }
        foreach (Unit unit in playerUnit)
        {
            if (unit.health<unit.life)
            {
                GameObject newBtn = Instantiate(UnitBtnBluePrint, PannelUnitList.transform);
                newBtn.transform.GetChild(1).GetComponent<TMP_Text>().text = unit.codeName;
                newBtn.transform.GetChild(0).GetChild(0).GetComponent<Image>().color = hubController.Model.skinTones[unit.skinTone];
                newBtn.transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[unit.Portrait_hair];
                newBtn.transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[unit.Portrait_Face];
                newBtn.transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[unit.Portrait_Body];
                newBtn.GetComponent<Button>().onClick.AddListener(() => hubController.SelectUnitInHospital(unit));
            }
        }
    }
    public void UpdateDetailsUnitMedicBay(Unit u,HubController hubController) {
        if (u==null)
        {
            Portrait_details_MedicBay.color = Color.black;
            Portrait_details_MedicBay.transform.GetChild(0).GetComponent<Image>().color = Color.white;
            Portrait_details_MedicBay.transform.GetChild(1).GetComponent<Image>().sprite = null;
            Portrait_details_MedicBay.transform.GetChild(2).GetComponent<Image>().sprite = null;
            Portrait_details_MedicBay.transform.GetChild(3).GetComponent<Image>().sprite = null;
            CodeNameAndDetails_Details_MedicBay.text = "";
        }
        else
        {
            Portrait_details_MedicBay.color = hubController.Model.GetColorOfClass(u);
            Portrait_details_MedicBay.transform.GetChild(0).GetComponent<Image>().color = hubController.Model.skinTones[u.skinTone];
            Portrait_details_MedicBay.transform.GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[u.Portrait_hair];
            Portrait_details_MedicBay.transform.GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[u.Portrait_Face];
            Portrait_details_MedicBay.transform.GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[u.Portrait_Body];
            CodeNameAndDetails_Details_MedicBay.text = u.codeName + "\n" + "Health:" + u.health + "/" + u.life;
        }
        
    }
    public void UpdateListsCure(List<Unit> pansements, List<Unit> firstaid, List<Unit> paliatif, List<Unit> chiurgery, List<Unit> experimental,HubController hubController) {
        UpdateListCure(pansements, pansementUnitList, hubController);
        UpdateListCure(firstaid, firstaidUnitList, hubController);
        UpdateListCure(paliatif, paliatifCureUnitList, hubController);
        UpdateListCure(chiurgery, chiurgeryUnitList, hubController);
        UpdateListCure(experimental, experimentalUnitList,hubController);
    }
    public void UpdateListCure(List<Unit> unitInList,List<GameObject> UiListElements, HubController hubController) {
        int cptElementUi = 0;
        foreach (Unit u in unitInList)
        {
            UiListElements[cptElementUi].GetComponent<Image>().color = hubController.Model.GetColorOfClass(u);
            UiListElements[cptElementUi].transform.GetChild(0).GetChild(0).GetComponent<Image>().color = hubController.Model.skinTones[u.skinTone];
            UiListElements[cptElementUi].transform.GetChild(0).GetChild(1).GetComponent<Image>().sprite = hubController.Model.Portraits_Hair[u.Portrait_hair];
            UiListElements[cptElementUi].transform.GetChild(0).GetChild(2).GetComponent<Image>().sprite = hubController.Model.Portraits_Face[u.Portrait_Face];
            UiListElements[cptElementUi].transform.GetChild(0).GetChild(3).GetComponent<Image>().sprite = hubController.Model.Portraits_Body[u.Portrait_Body];
            cptElementUi++;
        }
    }
    #endregion
    [Header("Prison")]
    #region Prison
    public GameObject PrisonPannel;
    #endregion
    [Header("Garage")]
    #region Garage
    public GameObject GaragePannel;
   

    #endregion

}
