﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(menuName ="Hub/Quest")]
public class quest :ScriptableObject
{
    public string questName;
    public string questDescription;
    public string questContent;
    public int questReward;
    public enum questTypes {kill,save,destroy};
    public questTypes TestType;
    public int QuestTypeId;
}
