﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[CreateAssetMenu(menuName = "Hub/Unit")]
public class Unit : ScriptableObject
{
    public string codeName;
    public string firstName;
    public string lastName;
    public string TragicBackStory;
    public int Portrait_hair;
    public int Portrait_Face;
    public int Portrait_Body;
    public int skinTone;
    public int nbOfInventorySlot;

    public int force = 2;
    public int intel =3;
    public List<EquipementItem> UnitItems=new List<EquipementItem>();


    public int life;
    public int health;
    public enum Class {Operateur,Scout,Gunner,Medic,Mechanic,Grenadier,Sniper,Pilot,Vehicules}
    public Class UnitClass = Class.Operateur;
    
    public Unit() {
        this.firstName = "test";
        this.lastName = "testing";
        this.TragicBackStory = "woman hater";     
    }
    public void GetMyInfos() {
        this.codeName = "code: " +Random.Range(100, 999);
        this.UnitClass = (Class)Enum.GetValues(typeof(Class)).GetValue(Random.Range(0, Enum.GetValues(typeof(Class)).Length));
        nbOfInventorySlot= Random.Range(1, 4);
        this.life = Random.Range(20, 100);
        this.health = Random.Range(1, life);
    }
    public void GetMyAvatar(int hair,int body,int face, int skin) {
        this.Portrait_hair = hair;
        this.Portrait_Body = body;
        this.Portrait_Face=face;
        this.skinTone = skin;
    }

    public string GetStatUnitsString()
    {
        return "Force:" + force + "\n" + "Intelligence:" + intel;
    }
    public void AddItem(EquipementItem item) {
        if (UnitItems.Count<nbOfInventorySlot)
        {
            UnitItems.Add(item);
        }
    }
}
