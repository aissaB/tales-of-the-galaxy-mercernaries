﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;

[Serializable]
public class QuestFIle 
{
    public string questName;
    public string questDescription;
    public string questContent;
    public int    questReward;
    public int QuestTypeId;
}
