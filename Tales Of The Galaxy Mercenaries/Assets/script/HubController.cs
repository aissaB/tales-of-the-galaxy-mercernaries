﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HubController : MonoBehaviour
{
    public HubVue Vue;
    public HubModel Model;
    private quest currentActiveQuest;
    private quest consultedQuest;
    private Unit consultedRecruit;
    private Unit consultedUnit;
    private Unit consultedSquadMember;
    
    
    private EquipementItem consultedItem_Vendor;
    private EquipementItem consultedItem_Player;
    private EquipementItem CurrentItemToassignToUnit;
    private Unit CurrentUnitToAssign;

    private Unit ConsultedUnitHopital;


    public quest CurrentActiveQuest
    {
        get
        {
            return currentActiveQuest;
        }
        set
        {
            
            currentActiveQuest = value;
        }
    }
    public Unit ConsultedRecruit
    {
        get { return consultedRecruit; }
        set {
            Vue.BtnMangement(3, (value != null));
            consultedRecruit = value; 
        }
    }
    public quest ConsultedQuest {
        get { return consultedQuest; }
        set {
            Vue.BtnMangement(0, (value != null));
            consultedQuest = value; 
        }
    }
    public Unit ConsultedUnit
    {
        get { return consultedUnit; }
        set {
            Vue.BtnMangement(1, (value != null));
            Vue.BtnMangement(4, (value != null));
            consultedUnit = value; }
    }
    public Unit ConsultedSquadMember {
        get { return consultedSquadMember; }
        set {
            Vue.BtnMangement(2, (value != null));
            consultedSquadMember = value; 
        }
    }
    public EquipementItem ConsultedItem_Vendor
    {
        get => consultedItem_Vendor;
        set {
            consultedItem_Player = null;
            Vue.HideItemDetailVendor();
            Vue.BuyButton.interactable = true;
            consultedItem_Vendor = value; 
        }
    }
    public EquipementItem ConsultedItem_Player
    {
        get => consultedItem_Player;
        set {
            consultedItem_Vendor = null;
            Vue.HideItemDetailPlayerBuySell();
            Vue.SellButton.interactable = true;
            consultedItem_Player = value; 
        }
    }

    private void Start()
    {
        Model.Player_squad = new List<Unit>();
        ConsultedQuest = null;
        ConsultedRecruit = null;
        ConsultedSquadMember = null;
        ConsultedUnit = null;
        CurrentItemToassignToUnit = null;
        CurrentUnitToAssign = null;
        Vue.OnServiceBtnClick(0);
        AddItemToVendorList();
        Model.GenerateRecruits();
        LoadQuests();
        LoadRecruits();
        Vue.UpdateEverythingUnits(Model.Player_units,this);
    }
    private void LoadQuests() {
        Vue.UpdateQuests(Model.allQuest, this);
    }
    public void ShowQuestInfo(quest q)
    {
        Vue.UpdateDescriptionQuest(q.questDescription);
        ConsultedQuest = q;
    }
    public void TakeShownQuest() {
        currentActiveQuest = ConsultedQuest;
        Vue.OnServiceBtnClick(0);
        Model.SaveQuestPrepartion(Model.Player_squad,currentActiveQuest);
    }
    private void LoadRecruits() {
        foreach (Unit item in Model.AvailibleRecruits)
        {
            item.GetMyInfos();
            item.codeName = Model.GetCodeName();
            item.GetMyAvatar(Model.GetAvatarPart(0), Model.GetAvatarPart(1), Model.GetAvatarPart(2), Model.GetAvatarPart(3));
        }
        Vue.UpdateListRecruits(Model.AvailibleRecruits, this);
        
    }
    public void ShowRecruitInfo(Unit r)
    {
        Vue.UpdateDescriptionRecruit(r.TragicBackStory);
        ConsultedRecruit = r;
    }
    public void ClickOnSquadMember(Unit S) {
        ConsultedSquadMember = S;
    }
    public void TakeShownRecruit() {
        Model.Player_units.Add(ConsultedRecruit);
        Model.AvailibleRecruits.Remove(ConsultedRecruit);
        Vue.UpdateListRecruits(Model.AvailibleRecruits, this);
        Vue.UpdateEverythingUnits(Model.Player_units, this);
        ConsultedRecruit = null;
        Vue.UpdateListOfUnitInLogistic(Model.Player_units, this);
        //TO be removed
        Vue.UpdateEverythingUnits(Model.Player_units, this);
    }
    public void ShowUnitInfos(Unit r) {
        Vue.UpdateDescriptionUnit_PlayerUnitPannel(r.TragicBackStory);
        ConsultedUnit = r;
            }
    public void AddUnitToSquad() {
        Model.Player_squad.Add(ConsultedUnit);
        Model.Player_units.Remove(ConsultedUnit);
        ConsultedUnit = null;
        Vue.UpdateSquad(Model.Player_squad, this);
        Vue.UpdateEverythingUnits(Model.Player_units, this);
    }
    public void RemoveUnitFromSquad() {
        Model.Player_units.Add(ConsultedSquadMember);
        Model.Player_squad.Remove(ConsultedSquadMember);
        ConsultedSquadMember = null;
        Vue.UpdateEverythingUnits(Model.Player_units, this);
        Vue.UpdateSquad(Model.Player_squad, this);
    }
    public void RemoveUnitFromPool() {
        Model.Player_units.Remove(ConsultedUnit);
        ConsultedUnit = null;
        Vue.UpdateEverythingUnits(Model.Player_units, this);
        Vue.UpdateDescriptionUnit_PlayerUnitPannel("");
    }
    public void AddItemToVendorList() {
        Vue.UpdateItemsVendorSide(Model.Items_Vendor, this);
    }
    public void SelectedItemVendorList(EquipementItem ei) {
        ConsultedItem_Vendor = ei;
        Vue.ShowItemDetailvendor(ei);
    }
    public void SelectedItemPlayerList(EquipementItem ei) {
        ConsultedItem_Player = ei;
        Vue.ShowItemDetailPlayerInventory(ei);
    }
    public void BuyItem() {
        for (int i = 0; i < Model.Items_Vendor.Count; i++)
        {
            if (Model.Items_Vendor[i].itemName == ConsultedItem_Vendor.itemName)
            {
                Model.Items_Vendor.RemoveAt(i);
                i = 200000;
            }
        }
        Model.Items_Player.Add(ConsultedItem_Vendor);
        Vue.UpdateItemsPlayerSide(Model.Items_Player, this);
        Vue.UpdateItemsVendorSide(Model.Items_Vendor, this);
        ConsultedItem_Vendor = null;
        Vue.HideItemDetailVendor();
        Vue.BuyButton.interactable = false;
    }
    public void SellItem() {
        for (int i = 0; i < Model.Items_Player.Count; i++)
        {
            if (Model.Items_Player[i].itemName == ConsultedItem_Player.itemName)
            {
                Model.Items_Player.RemoveAt(i);
                i = 200000;
            }
        }
        Model.Items_Vendor.Add(ConsultedItem_Player);
        Vue.UpdateItemsVendorSide(Model.Items_Vendor, this);
        Vue.UpdateItemsPlayerSide(Model.Items_Player, this);
        ConsultedItem_Player = null;
        Vue.HideItemDetailPlayerBuySell();
        Vue.SellButton.interactable = false;
    }
    public void SelectItemToEquip(EquipementItem item)
    {
        CurrentItemToassignToUnit = item;
        AddItemToUnit();
    }
    public void ShowUnitInfosInventory(Unit item)
    {
        CurrentUnitToAssign = item;
        Vue.UpdateUnitInfosInventory(Model.GetColorOfClass(item), Model.skinTones[item.skinTone], Model.Portraits_Hair[item.Portrait_hair], Model.Portraits_Face[item.Portrait_Face], Model.Portraits_Body[item.Portrait_Body],item.codeName,item.GetStatUnitsString());
        Vue.UpdateBtnEquippedItemByUnit(item.UnitItems);
        Vue.UpdateListOfPlayerItemsLogistic(Model.Items_Player, this, CurrentUnitToAssign);
    }
    public void AddItemToUnit()
    {
        for (int i = 0; i < Model.Player_units.Count; i++)
        {
            if (Model.Player_units[i].codeName==CurrentUnitToAssign.codeName) {
                Model.Player_units[i].AddItem(CurrentItemToassignToUnit);
                Model.Items_Player.Remove(CurrentItemToassignToUnit);
                Vue.UpdateBtnEquippedItemByUnit(Model.Player_units[i].UnitItems);
                Vue.UpdateListOfPlayerItemsLogistic(Model.Items_Player,this, CurrentUnitToAssign);
            }
        }
        
    }
    public void RemoveItemFromUnit(int id) {
        for (int i = 0; i < Model.Player_units.Count; i++)
        {
            if (Model.Player_units[i].codeName == CurrentUnitToAssign.codeName)
            {
                Model.Items_Player.Add(Model.Player_units[i].UnitItems[id]);
                Model.Player_units[i].UnitItems.RemoveAt(id);
                Vue.UpdateBtnEquippedItemByUnit(Model.Player_units[i].UnitItems);
                Vue.UpdateListOfPlayerItemsLogistic(Model.Items_Player, this, CurrentUnitToAssign);
            }
        }
    }
    public void SelectUnitInHospital(Unit u) {
        ConsultedUnitHopital = u;
        Vue.UpdateDetailsUnitMedicBay(u, this);
    }
    public void AddUnitToHealthSection(int lvl) {
        switch (lvl)
        {
            case 0:
                Model.unitsInPasements.Add(ConsultedUnitHopital);
                break;
            case 1:
                Model.unitsInFirstaid.Add(ConsultedUnitHopital);
                break;
            case 2:
                Model.unitsInPaliatif.Add(ConsultedUnitHopital);
                break;
            case 3:
                Model.unitsInChiurgie.Add(ConsultedUnitHopital);
                break;
            case 4:
                Model.unitsInExperimental.Add(ConsultedUnitHopital);
                break;
        }
        Model.Player_units.Remove(ConsultedUnitHopital);
        Vue.UpdateListsCure(Model.unitsInPasements, Model.unitsInFirstaid, Model.unitsInPaliatif, Model.unitsInChiurgie, Model.unitsInExperimental, this);
        Vue.UpdateEverythingUnits(Model.Player_units, this);
        ConsultedUnitHopital = null;
        Vue.UpdateDetailsUnitMedicBay(null, this);
    }
    
}
