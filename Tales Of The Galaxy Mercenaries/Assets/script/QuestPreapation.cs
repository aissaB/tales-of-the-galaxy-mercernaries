﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using UnityEngine;

[Serializable]
public class QuestPreapation 
{

    public List<UnitFile> Squad;
    public QuestFIle Quest;
public bool ReadyToLaunch;
    public QuestPreapation(List<Unit> squad,quest q) {
        Squad = new List<UnitFile>();
        ReadyToLaunch = false;
        foreach (Unit item in squad)
        {
            UnitFile Uf = new UnitFile();
            Uf.codeName = item.codeName;
            Uf.firstName= item.firstName; 
            Uf.lastName= item.lastName;
            Uf.TragicBackStory= item.TragicBackStory;
            Uf.Portrait_hair= item.Portrait_hair;
            Uf.Portrait_Face= item.Portrait_Face;
            Uf.Portrait_Body= item.Portrait_Body;
            Uf.skinTone =item.skinTone;
            Squad.Add(Uf);
        }
        Quest = new QuestFIle();
        Quest.questName = q.questName;
        Quest.questDescription=q.questDescription;
        Quest.questContent=q.questContent;
        Quest.questReward=q.questReward;
        Quest.QuestTypeId = q.QuestTypeId;
    }         
              
}             
              
